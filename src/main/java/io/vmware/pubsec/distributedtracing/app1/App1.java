package io.vmware.pubsec.distributedtracing.app1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class App1 {

	public static void main(String[] args) {
		SpringApplication.run(App1.class, args);
	}
	
	@Bean RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

}

@RestController
class HelloWorldSampleA {
	
	private static Logger LOG = LoggerFactory.getLogger(HelloWorldSampleA.class);
	@Autowired RestTemplate restTemplate;
	
	@Value("${app2.url}") String downstreamUrl;
	
	@GetMapping("/")
	public String main() {
		LOG.info("Invoking Service B...{}", downstreamUrl);
		return restTemplate.getForObject(downstreamUrl, String.class);
	}
}
